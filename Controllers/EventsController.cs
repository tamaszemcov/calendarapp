﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CalendarApp.DAL;
using CalendarApp.Models;

namespace CalendarApp.Controllers
{
    public class EventsController : ApiController
    {
        private CalendarAppContext db = new CalendarAppContext();

        // GET: api/Events
        // All the Events from a user
        public EventDTO[] GetEvents([FromUri] int userID)
        {
            var events = from e in db.Events
                         where e.OwnerID == userID
                         select new EventDTO()
                         {
                             EventID = e.EventID,
                             EventName = e.EventName,
                             StartDateTime = e.StartDateTime,
                             EndDateTime = e.EndDateTime,
                             EventDescription = e.EventDescription,
                             Important = e.Important,
                             Place = e.Place,
                             OwnerID = e.OwnerID,
                             Color = e.Tag.Color,
                             TagName = e.Tag.TagName,
                             TagID = e.TagID
                             
                         };

            return events.ToArray();
        }

        // POST: api/Events
        public void PostEvent([FromBody] EventDTO e)
        {
            db.Events.Add(new Event
            {
                EventID = e.EventID,
                EventName = e.EventName,
                StartDateTime = e.StartDateTime,
                EndDateTime = e.EndDateTime,
                EventDescription = e.EventDescription,
                Important = e.Important,
                Place = e.Place,
                OwnerID = e.OwnerID,
                TagID = e.TagID,

                Owner = db.Users.Single(u => u.UserID == e.OwnerID),
                Tag = db.Tags.Single(t => t.TagID == e.TagID)

               
            });
            db.SaveChanges();
        }



        //// GET: api/Events/5
        //[ResponseType(typeof(Event))]
        //public IHttpActionResult GetEvent(int id)
        //{
        //    Event @event = db.Events.Find(id);
        //    if (@event == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(@event);
        //}

        //// PUT: api/Events/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutEvent(int id, Event @event)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != @event.EventID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(@event).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!EventExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}



        //// DELETE: api/Events/5
        //[ResponseType(typeof(Event))]
        //public IHttpActionResult DeleteEvent(int id)
        //{
        //    Event @event = db.Events.Find(id);
        //    if (@event == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Events.Remove(@event);
        //    db.SaveChanges();

        //    return Ok(@event);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //private bool EventExists(int id)
        //{
        //    return db.Events.Count(e => e.EventID == id) > 0;
        //}
    }
}