﻿using System;
using System.Collections.Generic;
using System.Linq;
using CalendarApp.DAL;
using CalendarApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace CalendarApp.Controllers
{
    public class AuthenticationsController : ApiController
    {
        private CalendarAppContext db = new CalendarAppContext();

        // POST: api/Authentications
        // authenticate login
        public AuthenticationResponseDTO PostAuthentication([FromBody] AuthenticationDTO a)
        {
            AuthenticationResponseDTO responseDTO;

            if (db.Users.Where(u => u.Email == a.UserName).FirstOrDefault() == null)
            {
                responseDTO = new AuthenticationResponseDTO()
                {
                    Success = false,
                    UserId = 0,
                    Message = "E-mail not found"
                };
            }
            else
            {
                if (db.Users.Where(u => u.Email == a.UserName).FirstOrDefault().Password == a.Password)
                {
                    responseDTO = new AuthenticationResponseDTO()
                    {
                        Success = true,
                        UserId = db.Users.Where(u => u.Email == a.UserName).FirstOrDefault().UserID,
                        Message = "Authentication success"
                    };
                }
                else
                {
                    responseDTO = new AuthenticationResponseDTO()
                    {
                        Success = false,
                        UserId = 0,
                        Message = "Password is incorrect"
                    };
                }
            }

            return responseDTO;

        }

        // PUT: api/Authentications
        // add new user
        public void PutAuthentication([FromBody] UserDTO u)
        {
            db.Users.Add(new User
            {
                Email = u.Email,
                Password = u.Password,
                Name = u.Name,
                Image = u.Image,
                
                

            });
            db.SaveChanges();

        }
    }
}