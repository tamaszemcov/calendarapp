﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CalendarApp.Models;
using CalendarApp.DAL;

namespace CalendarApp.Controllers
{
    public class TagsController : ApiController
    {
        private CalendarAppContext db = new CalendarAppContext();

        // GET: api/Tags
        // All the Tags from a user
        public TagDTO[] GetTags([FromUri] int userID)
        {
            var tags = from t in db.Tags
                       where t.OwnerID == userID
                       select new TagDTO()
                       {
                           TagID = t.TagID,
                           TagName = t.TagName,
                           Color = t.Color,
                           OwnerID = t.OwnerID                      
                         };

            return tags.ToArray();
        }

        // POST: api/Tags
        public void PostTag([FromBody] TagDTO t)
        {
            db.Tags.Add(new Tag
            {
                TagName = t.TagName,
                Color = t.Color,
                OwnerID = t.OwnerID,
                Owner = db.Users.Single(u => u.UserID == t.OwnerID)
            
            });
            db.SaveChanges();
        }


        // DELETE: api/Tags
        public void DeleteTag([FromUri] int tagID)
        {
            db.Tags.Remove(db.Tags.Find(tagID));
            db.SaveChanges();
        }
    }
}
