﻿using CalendarApp.DAL;
using CalendarApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace CalendarApp.Controllers
{
    public class UsersController : ApiController
    {
        private CalendarAppContext db = new CalendarAppContext();

        // GET: api/Users
        // One User with userID
        public UserDTO[] GetUsers([FromUri] int userID)
        {
            var users = from u in db.Users
                        where u.UserID == userID
                        select new UserDTO()
                        {
                            UserID = u.UserID,
                            Name = u.Name,
                            Email = u.Email,
                            Password = u.Password,
                            Image = u.Image
                        };

            return users.ToArray();
        }

        

        // POST: api/Users
        // update user
        public void PostUser([FromBody] UserDTO u)
        {
            db.Users.Find(u.UserID).Name = u.Name;
            db.Users.Find(u.UserID).Email = u.Email;
            db.Users.Find(u.UserID).Password = u.Password;
            db.Users.Find(u.UserID).Image = u.Image;

            db.SaveChanges();
        }

    }   
}
