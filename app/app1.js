﻿var calendarApp = angular.module("calendarApp", []);

calendarApp.controller("UserController", function UserController($scope) {

    var imgSrc;
    var userImg = 2;

    switch(userImg) {
        case 1:
            imgSrc = "https://placeholdit.imgix.net/~text?txtsize=30&txt=A&w=64&h=64";
            break;
        case 2:
            imgSrc = "https://placeholdit.imgix.net/~text?txtsize=30&txt=B&w=64&h=64";
            break;
        case 3:
            imgSrc = "https://placeholdit.imgix.net/~text?txtsize=30&txt=C&w=64&h=64";
            break;
        case 4:
            imgSrc = "https://placeholdit.imgix.net/~text?txtsize=30&txt=D&w=64&h=64";
            break;
        
    }        

    $scope.user = {
        userId: 1,
        name: "Test User",
        email: "test.user@test.com",
        password: "testpassword",
        image: imgSrc
    };

    $scope.editUser = {};

    $scope.userEditOpen = function () {
        $scope.editUser = {
            userId: $scope.user.userId,
            name: $scope.user.name,
            email: $scope.user.email,
            password: $scope.user.password,
            image: $scope.user.image
        };
    };

    $scope.changeUser = function () {
        $scope.user.userId = $scope.editUser.userId;
        $scope.user.name = $scope.editUser.name;
        $scope.user.email = $scope.editUser.email;
        $scope.user.password = $scope.editUser.password;
        $scope.user.image = $scope.editUser.image;
    };

   
    

});



calendarApp.controller("TagsController", function TagsController($scope) {
    $scope.tags = [
        {
            tagId:  1,
            name: "testing1",
            color: "https://placeholdit.imgix.net/~text?txtsize=5&bg=99fd99&txtclr=000000&txt=&w=20&h=20&txtpad=1"
        },
        {
            tagId: 2,
            name: "testing2",
            color: "https://placeholdit.imgix.net/~text?txtsize=5&bg=DC143C&txtclr=000000&txt=&w=20&h=20&txtpad=1"
        },
        {
            tagId: 3,
            name: "testin3g",
            color: "https://placeholdit.imgix.net/~text?txtsize=5&bg=4169E1&txtclr=000000&txt=&w=20&h=20&txtpad=1"
        },
        {
            tagId: 4,
            name: "testing4",
            color: "https://placeholdit.imgix.net/~text?txtsize=5&bg=EEEE00&txtclr=000000&txt=&w=20&h=20&txtpad=1"
        },
        {
            tagId: 5,
            name: "testing5",
            color: "https://placeholdit.imgix.net/~text?txtsize=5&bg=FF8000&txtclr=000000&txt=&w=20&h=20&txtpad=1"
        } 
    ];
        
    $scope.newTagColor = "https://placeholdit.imgix.net/~text?txtsize=5&bg=DC143C&txtclr=000000&txt=&w=20&h=20&txtpad=1";

    $scope.addTag = function () {
        if ($scope.newTagName != null) {
            $scope.tags.push({
                tagId: 11,
                name: $scope.newTagName,
                color: $scope.newTagColor
            });
            $scope.newTagName = null;
        }
    };
    
    $scope.deleteTag = function (item) {
        var index = $scope.tags.indexOf(item);
        $scope.tags.splice(index, 1);
    };

});

calendarApp.service("EventsProvider", function () {

    var events = [
        {
            eventId: 1,
            name: "event1",
            desciption: "event1 description",
            place: "event1 place",
            startDateTime: new Date(2016,10,10,12,0,0,0),
            endDateTime: new Date(2016, 10, 10, 14, 0, 0, 0),
            important: false,
            tag: {
                tagId: 1,
                name: "testing1",
                color: "https://placeholdit.imgix.net/~text?txtsize=5&bg=99fd99&txtclr=000000&txt=&w=20&h=20&txtpad=1"
            }
        }
    ];

    this.calendarData = [
        {
            name: "2016. october",
            year: 2016,
            month: 10,
            days: [
                {
                    number: 1,
                    events: [
                        {
                            eventId: 1,
                            name: "event1",
                            desciption: "event1 description",
                            place: "event1 place",
                            important: false,
                            tag: {
                                tagId: 1,
                                name: "testing1",
                                color: "99fd99"
                            }
                        },
                        {
                            eventId: 2,
                            name: "event2",
                            desciption: "event2 description",
                            place: "event2 place",
                            important: false,
                            tag: {
                                tagId: 2,
                                name: "testing2",
                                color: "DC143C"
                            }
                        }
                    ]
                },
                {
                    number: 2,
                    events: []
                }
            ]
        }
    ];


});


calendarApp.service("DateProvider", function () {                     
                                                                      
        this.dayss = {                                                
            y2016: {
                jan: [0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
                feb: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29],
                mar: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
                apr: [0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
                may: [0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
                jun: [0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],

                jul: [0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
                aug: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
                sep: [0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
                oct: [0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
                nov: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
                dec: [0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
            },
            y2017: {
                jan: [0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
                feb: [0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28],
                mar: [0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
                apr: [0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
                may: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
                jun: [0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],

                jul: [0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
                aug: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
                sep: [0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
                oct: [0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
                nov: [0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
                dec: [0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
            }
        };

        this.day = [null, null, null, null, null, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
        
        this.months = [
            {
                name: "2016. October",
                days: [null, null, null, null, null, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
            },
            {
                name: "2016. November",
                days: [null,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
            }
        ];

    });

calendarApp.controller("CalendarController", ["$scope", "DateProvider", "EventsProvider", function CalendarController($scope, DateProvider, EventsProvider) {

    var monthIndex = 0;
    var monthMax = 1;

    $scope.calendarDays = EventsProvider.calendarData[monthIndex].days;
    $scope.calendarMonth = EventsProvider.calendarData[monthIndex].name;


    $scope.previousMonth = function () {
        if (monthIndex > 0) {
            monthIndex--;
            $scope.calendarDays = DateProvider.months[monthIndex].days;
            $scope.calendarMonth = DateProvider.months[monthIndex].name;
        }
    };

    $scope.nextMonth = function () {
       if (monthIndex < monthMax) {
           monthIndex++;
           $scope.calendarDays = DateProvider.months[monthIndex].days;
           $scope.calendarMonth = DateProvider.months[monthIndex].name;
       }
    };

    $scope.tags = [
       {
           tagId: 1,
           name: "testing1",
           color: "99fd99"
       },
       {
           tagId: 2,
           name: "testing2",
           color: "DC143C"
       },
       {
           tagId: 3,
           name: "testin3g",
           color: "4169E1"
       },
       {
           tagId: 4,
           name: "testing4",
           color: "EEEE00"
       },
       {
           tagId: 5,
           name: "testing5",
           color: "FF8000"
       }
    ];


    $scope.newEvent = {};


    $scope.addEventTable = function (index) {
        $scope.calendarDays[index].events.push({
            eventId: 1,
            name: $scope.newEvent.name,
            desciption: $scope.newEvent.desciption,
            place: $scope.newEvent.place,
            important: $scope.newEvent.important,
            tag: {
                tagId: 1,
                name: "testing1",
                color: $scope.newEvent.tag.color
            }
        });

        $scope.newEvent = {};
        $("#addEventTableModal").modal("toggle");
    };



}]);


    