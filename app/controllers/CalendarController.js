﻿angular.module("calendarApp")
    .controller("CalendarController", ["$scope", "DateProvider", "EventsProvider", "$http", "$rootScope", function CalendarController($scope, DateProvider, EventsProvider, $http, $rootScope) {

        var events = [];

        $http({
            method: 'GET',
            url: '/api/events?userID=' + $rootScope.globals.currentUser.userId
        }).then(function successCallback(response) {
            console.log(response);
            
            for (var i = 0; i < response.data.length; i++) {
                var eventColor;
                switch (response.data[i].Color) {
                    case 1:
                        eventColor = "#99fd99";
                        break;
                    case 2:
                        eventColor = "#DC143C";
                        break;
                    case 3:
                        eventColor = "#4169E1";
                        break;
                    case 4:
                        eventColor = "#EEEE00";
                        break;
                    case 5:
                        eventColor = "#FF8000";
                        break;
                }
                
            
                events.push({
                    title: response.data[i].EventName,
                    allDay: false,
                    color: eventColor,
                    start: response.data[i].StartDateTime,
                    end: response.data[i].EndDateTime,

                    description: response.data[i].EventDescription,
                    important: response.data[i].Important,
                    place: response.data[i].Place,
                    tagId: response.data[i].TagID,
                    tagName: response.data[i].TagName
                });
            }

            console.log(events);
            
            // calendar render
            $('#calendar').fullCalendar({
                header: {
                    left: 'today prev,next',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                firstDay: 1,
                events: events,

                dayClick: function (date, jsEvent, view) {
                    $scope.newEvent.startDate = date.format("YYYY-MM-DD[T]HH:mm:ss");
                    $("#addEventTableModal").modal("toggle");
                },
                eventClick: function(calEvent, jsEvent, view) {

                    alert('Event: ' + calEvent.title);

                }


            });


        }, function errorCallback(response) {
            console.log("http error");
        });


        $scope.newEvent = {};

        $scope.addEvent = function() {
            events.push({
                title: $scope.newEvent.name,
                allDay: false,
                color: $scope.newEvent.color,
                start: $scope.newEvent.startDate,
                end: $scope.newEvent.endDate,

                description: $scope.newEvent.description,
                important: $scope.newEvent.important,
                place: $scope.newEvent.place,
                tagId: 11,
                tagName: ""
            });

            $('#calendar').fullCalendar('renderEvent', $scope.newEvent);

            $scope.newEvent = {};
            $("#addEventTableModal").modal("toggle");

           
        };
       



                 
        
        


}]);


//[{
//    allDay:false,
//    color:"#99fd99",
//    description:"event1 description",
//    end:"2016-10-10T14:15:00",
//    important:true,
//    place:"place1",
//    start:"2016-10-10T12:15:00",
//    tagId:1,
//    tagName:"tag1",
//    title:"event1"
//}]
