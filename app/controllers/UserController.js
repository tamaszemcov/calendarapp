﻿angular.module("calendarApp")
    .controller("UserController", ["$scope", "UserProvider", "$http", "$rootScope", function UserController($scope, UserProvider, $http, $rootScope) {

    

    //$scope.user = UserProvider.getUser();

    //$scope.editUser = {};

    $scope.userEditOpen = function () {
        $scope.editUser = {
            userId: $scope.user.userId,
            name: $scope.user.name,
            email: $scope.user.email,
            password: $scope.user.password,
            image: $scope.user.image
        };
    };

    $scope.changeUser = function () {
        $scope.user.userId = $scope.editUser.userId;
        $scope.user.name = $scope.editUser.name;
        $scope.user.email = $scope.editUser.email;
        $scope.user.password = $scope.editUser.password;
        $scope.user.image = $scope.editUser.image;


        switch ($scope.editUser.image) {
            case "https://placeholdit.imgix.net/~text?txtsize=30&txt=A&w=64&h=64":
                $scope.user.image = 0;
                break;
            case "https://placeholdit.imgix.net/~text?txtsize=30&txt=B&w=64&h=64":
                $scope.user.image = 1;
                break;
            case "https://placeholdit.imgix.net/~text?txtsize=30&txt=C&w=64&h=64":
                $scope.user.image = 2;
                break;
            case "https://placeholdit.imgix.net/~text?txtsize=30&txt=D&w=64&h=64":
                $scope.user.image = 3;
                break;

        }

        $http({
            method: 'POST',
            url: '/api/users',
            headers: {'Content-Type': 'application/json; charset=utf-8'},
            data: $scope.user
        }).then(function successCallback(response) {
            $scope.user.image = $scope.editUser.image;
        }, function errorCallback(response) {
            
        });


        


        //UserProvider.editUser($scope.editUser.userId, $scope.editUser.name, $scope.editUser.email, $scope.editUser.password, $scope.editUser.image);
        //$scope.user = UserProvider.getUser();

    };

   
    //$scope.works = UserProvider.getResponse();
    //console.log($scope.works);
    //console.log(UserProvider.getResponse());

    $http({
        method: 'GET',
        url: '/api/users?userID=' + $rootScope.globals.currentUser.userId
    }).then(function successCallback(response) {
        $scope.user = response.data[0];
        $scope.user.userId = response.data[0].UserID;
        $scope.user.name = response.data[0].Name;
        $scope.user.email = response.data[0].Email;
        $scope.user.password = response.data[0].Password;

        var imageUrl;
        switch (response.data[0].Image) {
            case 0:
                imageUrl = "https://placeholdit.imgix.net/~text?txtsize=30&txt=A&w=64&h=64";
                break;
            case 1:
                imageUrl = "https://placeholdit.imgix.net/~text?txtsize=30&txt=B&w=64&h=64";
                break;
            case 2:
                imageUrl = "https://placeholdit.imgix.net/~text?txtsize=30&txt=C&w=64&h=64";
                break;
            case 3:
                imageUrl = "https://placeholdit.imgix.net/~text?txtsize=30&txt=D&w=64&h=64";
                break;

        }


        $scope.user.image = imageUrl;


    }, function errorCallback(response) {
        console.log("http error");
    });



}]);