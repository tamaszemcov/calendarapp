﻿angular.module("calendarApp")

.controller('LoginController',
    ['$scope', '$rootScope', '$location', 'AuthenticationService', '$http',
    function ($scope, $rootScope, $location, AuthenticationService, $http) {
        // reset login status
        AuthenticationService.ClearCredentials();

        $scope.login = function () {
            $scope.dataLoading = true;
            AuthenticationService.Login($scope.username, $scope.password, function (response) {
                if (response.Success) {
                    AuthenticationService.SetCredentials($scope.username, $scope.password, response.UserId);
                    $location.path('/');
                } else {
                    $scope.error = response.Message;
                    $scope.dataLoading = false;
                }
            });
        };

        $scope.signup = function () {
            $http({
                method: 'PUT',
                url: '/api/authentications',
                headers: { 'Content-Type': 'application/json; charset=utf-8' },
                data: {
                    UserID: 11,
                    Name: $scope.signupname,
                    Email: $scope.signupusername,
                    Password: $scope.signuppassword,
                    Image: 0                   
                }
            }).then(function successCallback(response) {
                $scope.signupname = "";
                $scope.signupusername = "";
                $scope.signuppassword = "";
            }, function errorCallback(response) {

            });
        }
    }]);