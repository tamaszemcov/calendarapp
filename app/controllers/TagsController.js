﻿angular.module("calendarApp")
    .controller("TagsController", ["$scope", "TagsProvider", "$http", "$rootScope", function TagsController($scope, TagsProvider, $http, $rootScope) {

        //$scope.tags = TagsProvider.getTags();
        $scope.tags = [];
        $http({
            method: 'GET',
            url: '/api/tags?userID=' + $rootScope.globals.currentUser.userId
        }).then(function successCallback(response) {
            for (var i = 0; i < response.data.length; i++) {

                var colorUrl;
                switch (response.data[i].Color) {
                    case 1:
                        colorUrl = "https://placeholdit.imgix.net/~text?txtsize=5&bg=99fd99&txtclr=000000&txt=&w=20&h=20&txtpad=1";
                        break;
                    case 2:
                        colorUrl = "https://placeholdit.imgix.net/~text?txtsize=5&bg=DC143C&txtclr=000000&txt=&w=20&h=20&txtpad=1";
                        break;
                    case 3:
                        colorUrl = "https://placeholdit.imgix.net/~text?txtsize=5&bg=4169E1&txtclr=000000&txt=&w=20&h=20&txtpad=1";
                        break;
                    case 4:
                        colorUrl = "https://placeholdit.imgix.net/~text?txtsize=5&bg=EEEE00&txtclr=000000&txt=&w=20&h=20&txtpad=1";
                        break;
                    case 5:
                        colorUrl = "https://placeholdit.imgix.net/~text?txtsize=5&bg=FF8000&txtclr=000000&txt=&w=20&h=20&txtpad=1";
                        break;

                }

                $scope.tags.push({
                    tagId: response.data[i].TagID,
                    name: response.data[i].TagName,
                    color: colorUrl
                });
            }

        }, function errorCallback(response) {
            console.log("http error");
        });

        $scope.newTagColor = "https://placeholdit.imgix.net/~text?txtsize=5&bg=DC143C&txtclr=000000&txt=&w=20&h=20&txtpad=1";

    $scope.addTag = function () {
        if ($scope.newTagName != null) {
            $scope.tags.push({
                tagId: 11,
                name: $scope.newTagName,
                color: $scope.newTagColor
            });
            //TagsProvider.addTag(11, $scope.newTagName, $scope.newTagColor);

            var colorNumber;
            switch ($scope.newTagColor) {
                case "https://placeholdit.imgix.net/~text?txtsize=5&bg=99fd99&txtclr=000000&txt=&w=20&h=20&txtpad=1":
                    colorNumber = 1;
                    break;
                case "https://placeholdit.imgix.net/~text?txtsize=5&bg=DC143C&txtclr=000000&txt=&w=20&h=20&txtpad=1":
                    colorNumber = 2;
                    break;
                case "https://placeholdit.imgix.net/~text?txtsize=5&bg=4169E1&txtclr=000000&txt=&w=20&h=20&txtpad=1":
                    colorNumber = 3;
                    break;
                case "https://placeholdit.imgix.net/~text?txtsize=5&bg=EEEE00&txtclr=000000&txt=&w=20&h=20&txtpad=1":
                    colorNumber = 4;
                    break;
                case "https://placeholdit.imgix.net/~text?txtsize=5&bg=FF8000&txtclr=000000&txt=&w=20&h=20&txtpad=1":
                    colorNumber = 5;
                    break;

            }




            $http({
                method: 'POST',
                url: '/api/tags',
                headers: { 'Content-Type': 'application/json; charset=utf-8' },
                data: {
                    TagID: $scope.tags,
                    TagName: $scope.newTagName,
                    Color: colorNumber,
                    OwnerID: $rootScope.globals.currentUser.userId
                }
            }).then(function successCallback(response) {
                //reget tags

                // /reget tags
            }, function errorCallback(response) {

            });

            $scope.newTagName = null;
        }
    };

    $scope.deleteTag = function (item) {
       

        $http({
            method: 'DELETE',
            url: '/api/tags?tagID=' + item.tagId
        }).then(function successCallback(response) {
            //TagsProvider.removeTag(item);
            var index = $scope.tags.indexOf(item);
            $scope.tags.splice(index, 1);
        }, function errorCallback(response) {

        });

        

    };

}]);