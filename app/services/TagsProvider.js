﻿angular.module("calendarApp")
    .service("TagsProvider", function ($http) {
        

        var tags = [
        {
            tagId: 1,
            name: "testing1",
            color: "https://placeholdit.imgix.net/~text?txtsize=5&bg=99fd99&txtclr=000000&txt=&w=20&h=20&txtpad=1"
        },
        {
            tagId: 2,
            name: "testing2",
            color: "https://placeholdit.imgix.net/~text?txtsize=5&bg=DC143C&txtclr=000000&txt=&w=20&h=20&txtpad=1"
        },
        {
            tagId: 3,
            name: "testin3g",
            color: "https://placeholdit.imgix.net/~text?txtsize=5&bg=4169E1&txtclr=000000&txt=&w=20&h=20&txtpad=1"
        },
        {
            tagId: 4,
            name: "testing4",
            color: "https://placeholdit.imgix.net/~text?txtsize=5&bg=EEEE00&txtclr=000000&txt=&w=20&h=20&txtpad=1"
        },
        {
            tagId: 5,
            name: "testing5",
            color: "https://placeholdit.imgix.net/~text?txtsize=5&bg=FF8000&txtclr=000000&txt=&w=20&h=20&txtpad=1"
        }
        ];

        


        this.getTags = function () {
            return tags;
        };

        this.addTag = function (newTagId, newTagName, newTagColor) {
            tags.push({
                tagId: newTagId,
                name: newTagName,
                color: newTagColor
            });
        };

        this.removeTag = function (item) {
            var index = tags.indexOf(item);
            tags.splice(index, 1);
        };


    });