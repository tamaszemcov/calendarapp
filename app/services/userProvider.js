﻿angular.module("calendarApp")
    .service("UserProvider", function ($http) {

        var user = {
            userId: 1,
            name: "Test User",
            email: "test.user@test.com",
            password: "testpassword",
            image: "https://placeholdit.imgix.net/~text?txtsize=30&txt=B&w=64&h=64"
        };


        this.getUser = function () {
            return user;
        };

        this.editUser = function (userId, name, email, password, image) {
            user.userId = userId;
            user.name = name;
            user.email = email;
            user.password = password;
            user.image = image;
        };

        this.getResponse = function (callBackFunction) {
  
            $http({
                method: 'GET',
                url: '/api/users?userID=1'
            }).then(function successCallback(response) {
                callBackFunction(response);
                
                
            }, function errorCallback(response) {
                console.log("http error");
            });

            

           
        };


    });