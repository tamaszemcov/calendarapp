﻿angular.module("calendarApp")
    .service("EventsProvider", function () {

    var events = [
        {
            eventId: 1,
            name: "event1",
            desciption: "event1 description",
            place: "event1 place",
            startDateTime: new Date(2016, 10, 10, 12, 0, 0, 0),
            endDateTime: new Date(2016, 10, 10, 14, 0, 0, 0),
            important: false,
            tag: {
                tagId: 1,
                name: "testing1",
                color: "https://placeholdit.imgix.net/~text?txtsize=5&bg=99fd99&txtclr=000000&txt=&w=20&h=20&txtpad=1"
            }
        }
    ];

    this.calendarData = [
        {
            name: "2016. october",
            year: 2016,
            month: 10,
            days: [
                {
                    number: 1,
                    events: [
                        {
                            eventId: 1,
                            name: "event1",
                            desciption: "event1 description",
                            place: "event1 place",
                            important: false,
                            tag: {
                                tagId: 1,
                                name: "testing1",
                                color: "99fd99"
                            }
                        },
                        {
                            eventId: 2,
                            name: "event2",
                            desciption: "event2 description",
                            place: "event2 place",
                            important: false,
                            tag: {
                                tagId: 2,
                                name: "testing2",
                                color: "DC143C"
                            }
                        }
                    ]
                },
                {
                    number: 2,
                    events: []
                }
            ]
        }
    ];


});