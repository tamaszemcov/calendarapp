﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarApp.Models
{
    public class Event
    {
        public int EventID { get; set; }
        public string EventName { get; set; }
        public string EventDescription { get; set; }
        public string Place { get; set; }

        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }

        public bool Important { get; set; }

        public int TagID { get; set; }
        public int OwnerID { get; set; }

        public virtual Tag Tag { get; set; }
        public virtual User Owner { get; set; }

    }
}