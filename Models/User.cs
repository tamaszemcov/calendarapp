﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarApp.Models
{
    public class User
    {
        public int UserID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Image { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
        public virtual ICollection<Event> Events { get; set; }


    }
}