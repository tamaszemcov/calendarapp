﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarApp.Models
{
    public class TagDTO
    {
        public int TagID { get; set; }
        public string TagName { get; set; }
        public int Color { get; set; }

        public int OwnerID { get; set; }
    }
}