﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using CalendarApp.Models;

namespace CalendarApp.DAL
{
    public class CalendarAppContext : DbContext
    {
        public CalendarAppContext () : base("CalendarAppContext")
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Event> Events { get; set; }
    }
}