﻿using CalendarApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarApp.DAL
{
    public class CalendarAppInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<CalendarAppContext>
    {
        protected override void Seed(CalendarAppContext context)
        {
            var users = new List<User>
            {
                new User {Name="test1", Email="test1@test.com", Password="test1", Image=0 },
                new User {Name="test2", Email="test2@test.com", Password="test2", Image=0 },
                new User {Name="test3", Email="test3@test.com", Password="test3", Image=0 },
                new User {Name="test4", Email="test4@test.com", Password="test4", Image=0 },
                new User {Name="test5", Email="test5@test.com", Password="test5", Image=0 }
            };

            users.ForEach(u => context.Users.Add(u));
            context.SaveChanges();

            var tags = new List<Tag>
            {
                new Tag {TagName="tag1", Color=1, OwnerID=1 },
                new Tag {TagName="tag2", Color=2, OwnerID=1 },
                new Tag {TagName="tag3", Color=3, OwnerID=1 },
                new Tag {TagName="tag4", Color=1, OwnerID=2 },
                new Tag {TagName="tag5", Color=1, OwnerID=3 },
                new Tag {TagName="tag6", Color=2, OwnerID=4 }
            };

            tags.ForEach(t => context.Tags.Add(t));
            context.SaveChanges();

            var events = new List<Event>
            {
                new Event {EventName="event1", EventDescription="event1 description", Place="place1", StartDateTime= new DateTime(2016,10,10,12,15,0), EndDateTime= new DateTime(2016,10,10,14,15,0), Important=true, OwnerID=1, TagID=1 },
                new Event {EventName="event2", EventDescription="event2 description", Place="place1", StartDateTime= new DateTime(2016,10,10,12,15,0), EndDateTime= new DateTime(2016,10,11,12,15,0), Important=false, OwnerID=1, TagID=2 },
                new Event {EventName="event3", EventDescription="event3 description", Place="place2", StartDateTime= new DateTime(2016,10,10,12,15,0), EndDateTime= new DateTime(2016,10,12,16,15,0), Important=false, OwnerID=1, TagID=3 },
                new Event {EventName="event4", EventDescription="event4 description", Place="place2", StartDateTime= new DateTime(2016,10,10,12,15,0), EndDateTime= new DateTime(2016,10,10,14,15,0), Important=true, OwnerID=2, TagID=4 },
                new Event {EventName="event5", EventDescription="event5 description", Place="place3", StartDateTime= new DateTime(2016,10,10,12,15,0), EndDateTime= new DateTime(2016,12,10,12,15,0), Important=false, OwnerID=3, TagID=5 }
            };

            events.ForEach(e => context.Events.Add(e));
            context.SaveChanges();



        }
    }
}